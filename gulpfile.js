const gulp = require('gulp');
const browser = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
// const pref = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const concat = require('gulp-concat');

const css = () => {
    return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'))
}

const js = () => {
    return gulp.src('./src/js/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./dist/js'))
}

const cleanDist = () => {
    return gulp.src('./dist', {read: false, allowEmpty: true})
    .pipe(clean())
}

const dev = () => {
    browser.init({
        server: {
            baseDir: "./",
            index: "./index.html"
        }
    });

    gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(css, js), (cb) => {
        browser.reload()
        cb();
    }))
    gulp.watch('./index.html', (next) => {
        browser.reload()
        next()
    })
}  

gulp.task('build',gulp.series(cleanDist, css,js))
gulp.task('clean', cleanDist)
gulp.task('dev', dev)