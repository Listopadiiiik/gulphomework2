const menu = document.querySelector('.menu');
const list = document.querySelector('.list');
const lastLine = document.querySelector('.line:last-child');
const lineOne = document.querySelector('.line')
const lineTwo = document.querySelector('.line:nth-child(2)');

menu.addEventListener('click', function(){
    lastLine.classList.toggle('remove');
    lineOne.classList.toggle('rotate');
    lineTwo.classList.toggle('rotate-reverse');
    list.classList.toggle('remove')
})
